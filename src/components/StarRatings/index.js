import React from 'react';
import './index.scss'

const StarRatings = ({ countstarts, value, inactiveColor, activeColor, onChange }) => {
      
    const stars = Array.from({length: countstarts}, () => '🟊')
    
    const handleChange = (value) => {
        onChange(value + 1);
      }
    return (
        <div className='star-container'>
            <h1> CALIFICA NUESTRO COMIC </h1>
            
            <div className='star-container__items'>    
                {stars.map((s, index) => {
                    let style = inactiveColor;
                        if (index < value) {
                        style = activeColor;
                    }
                    return (
                    <span   
                        key={index}
                        style={{color: style}}
                        onClick={()=>handleChange(index)}>{s}</span>
                    )
                })}
            </div>
                        
            
        </div>
    );
}


export default StarRatings;