import React from 'react'   
import './index.scss'

const ComicsApi = ({ title, image }) => {
    return(
        <div className='content'>
            <h1> {title} </h1>
            <img src={image} alt={image}/>
        </div>
    )
}

export default ComicsApi;