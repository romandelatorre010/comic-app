import React, { useState, useEffect } from 'react';
import ComicsApi from '../components/ComicsApi'
import StarRatings from '../components/StarRatings'

import './home.scss';

const Home = () => {
	
	const [Comics, setComics] = useState([])
	const [rating, setRating] = useState();

	useEffect(function(){
		fetch('https://xkcd.com/70/info.0.json')
			.then(res => res.json())
			.then(data => setComics(data))
	}, [])

	const handleChange = (value) => {
	  setRating(value);
	}

	const showAlert = () => {
		if (rating > 0) {
			alert('Gracias por tu opinión')
		}
		setRating() 
	}

  	return (
    	<div className="container">
			<ComicsApi
				title={Comics.title}
				image={Comics.img}
			/>
			<StarRatings
				countstarts={5}
				value={rating}
       			activeColor ={'#fcd937'}
       			inactiveColor={'#ddd'}
       			onChange={handleChange}
			/>
			{
				rating &&
				<button onClick={ () => showAlert()}>ENVIAR</button>
			}
    	</div>
  );
}

export default Home;
